﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Persistence.Models
{
    public class Burger
    {

        public enum TasteType
        {
            Awful,
            Meh,
            Ok,
            Great,
        }

        public enum TextureType
        {
            Soggy,
            Medium,
            Crisp,
        }

        public enum PresentationType
        {
            Garbage,
            Plain,
            Nice,
            Extravaganca,
        }


        public int ID { get; set; }
        public string Name { get; set; }
        public TasteType Taste { get; set; }
        public TextureType Texture { get; set; }
        public PresentationType Presentation { get; set; }

        public Restaurant Restaurant { get; set; }


    }
}

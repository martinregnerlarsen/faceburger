﻿using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Persistence.Models
{
    public class Restaurant
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string OpeningHours { get; set; }
        public Point Location { get; set; }

        public List<Burger> Burgers { get; set; }
    }
}

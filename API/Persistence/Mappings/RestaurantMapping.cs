﻿using Faceburger.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Persistence.Mappings
{
    public partial class RestaurantMapping : IEntityTypeConfiguration<Restaurant>
    {
        public void Configure(EntityTypeBuilder<Restaurant> builder) {
            builder
                .Property(b => b.Name)
                .IsRequired();

            builder
                .Property(b => b.OpeningHours);

            builder
                .Property(b => b.Location)
                .HasColumnType("geometry (point)")
                .IsRequired();

            builder
                .HasMany(b => b.Burgers)
                .WithOne(b => b.Restaurant);

            // Create spatial index on location
            builder.HasIndex(b => b.Location)
                .HasMethod("gist");
        }
    }
}

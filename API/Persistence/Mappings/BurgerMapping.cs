﻿using Faceburger.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Faceburger.Persistence.Mappings
{
    public partial class RestaurantMapping
    {
        public class BurgerMapping : IEntityTypeConfiguration<Burger>
        {
            public void Configure(EntityTypeBuilder<Burger> builder) {
                builder.Property(x => x.Name)
                    .IsRequired();

                builder.Property(x => x.Presentation)
                    .HasConversion<string>();

                builder.Property(x => x.Taste)
                    .HasConversion<string>();

                builder.Property(x => x.Texture)
                    .HasConversion<string>();

            }
        }
}

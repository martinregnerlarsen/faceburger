﻿using Faceburger.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCore.NamingConventions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Faceburger.Persistence
{
    public class FaceburgerDbContext : IdentityDbContext<IdentityUser>
    {
        
        public FaceburgerDbContext(DbContextOptions options): base(options) {

        }

        public DbSet<Restaurant> Restaurants { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseLowerCaseNamingConvention();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresExtension("postgis");
            modelBuilder.HasDefaultSchema("faceburger");
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DbContext).Assembly);
        }

    }

   
}

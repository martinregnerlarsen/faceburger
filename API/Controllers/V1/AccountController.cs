﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        [HttpPost("register")]
        public IActionResult Register() {
            return this.Unauthorized();
        }

        [HttpPost("authenticate")]
        public ActionResult<string> Authenticate([FromBody]AuthenticationInput input) {
            return Ok(input.Username);
        }
    }

    public class AuthenticationInput
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

﻿using Faceburger.Persistence;
using Faceburger.Persistence.Models;
using Faceburger.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {

        private IRestaurantService _restaurantService;
        private FaceburgerDbContext _dbContext;

        public RestaurantController(IRestaurantService restaurantService, FaceburgerDbContext dbContext) {
            _restaurantService = restaurantService;
            _dbContext = dbContext;
        }

        [HttpGet("get-near")]
        public async Task<ActionResult<List<ListOutputDto>>> getRestaurantsNear([FromQuery] ListInputDto input) {
            var result = await _restaurantService.GetResturantsNearAsync(input.Lat, input.Lon, input.Radius);

            return Ok(result.Select(x => new ListOutputDto {
                Id = x.ID,
                Lat = x.Location.Y,
                Lon = x.Location.X,
                Name = x.Name,
            }).ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Restaurant>> Get(int id) {

            // Load the restaurant
            var restaurant = _dbContext.Restaurants.Single(b => b.ID == id);
            
            // Explicit load the restaurants burgers
            await _dbContext.Entry(restaurant).Collection(b => b.Burgers).LoadAsync();

            return Ok(restaurant);
        }
    }


    public class ListInputDto
    {
        [Required, Range(-20026376.39, 20026376.39)]
        public double Lat { get; set; }
        [Required, Range(-20026376.39, 20026376.39)]
        public double Lon { get; set; }
        [Required, Range(1, 20000)]
        public double Radius { get; set; }
    }

    public class ListOutputDto
    {
        public int Id { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Name { get; set; }
    }
}

﻿using Faceburger.Persistence;
using Faceburger.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Services
{
    public class RestaurantService : IRestaurantService
    {

        private FaceburgerDbContext _dbContext;
        private ConnectionMultiplexer _redis;

        public RestaurantService(FaceburgerDbContext dbContext, ConnectionMultiplexer redis) {
            _dbContext = dbContext;
            _redis = redis;
        }

        
        public async Task<IQueryable<Restaurant>> GetResturantsNearAsync(double lat, double lon, double maxDistance) {
            // Redis is prefilled with restaurants - so use that to query by location.
            // Afterwards we'll query postgres full dataset for found restaurants
            var db = _redis.GetDatabase();
            var cached = await db.GeoRadiusAsync("resturants", lat, lon, maxDistance);
            var ids = Array.ConvertAll(cached, x => (int)x.Member);

            return _dbContext.Restaurants.Where(x => ids.Contains(x.ID));
        }
    }
}

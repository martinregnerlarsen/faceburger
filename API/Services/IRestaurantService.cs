﻿using Faceburger.Persistence.Models;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faceburger.Services
{
    public interface IRestaurantService
    {
        Task<IQueryable<Restaurant>> GetResturantsNearAsync(double lat, double lon, double maxDistance);
    }
}
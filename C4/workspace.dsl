workspace {

    model {
        user = person "User"
        faceburger = softwareSystem "Faceburger" {
            webapp = container "Web application"
            cloudfront = container "cloudfront"
            s3 = container "S3"
            api = container "Web API" {
                identityProvider = component "Identity provider"
                accountController = component "AccountController"
                restaurantController = component "RestaurantController"
                restaurantService = component "RestaurantService
                imageService = component "ImageService"
            }
            redis = container "Redis" "" "redis"
            database = container "Postgresql" "" "database"
        }

        user -> faceburger "Uses"
        // webapp -> api
        // mobile -> api
        api -> redis "Eventual caching"
        api -> database "reads from and writes to"
        api -> s3 "Writes images and thumbnails"
        webapp -> cloudfront "reads images and thumbnails"
        webapp -> accountController "Authenticate"
        webapp -> restaurantController "Make API calls (https/json)"
        accountController -> identityProvider "Get JWT Token"
        restaurantController -> restaurantService "Get restarant information"
        restaurantService -> database "Read and writes"
        restaurantService -> redis "Reads cached data and updates cache"
        restaurantService -> imageService "Stores images"
        imageService -> s3 "Stores images and thumbnails"
        cloudfront -> s3 "Reads images and thumbnails"
    }

    views {
        container faceburger {
            include *
            autoLayout
        }

        component api {
            include *
            autoLayout
        }

        styles {
            element database {
                shape "Cylinder"
            }
        }

        theme default
    }
}